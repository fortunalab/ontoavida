[![Build Status](https://travis-ci.org/fortunalab/ontoavida.svg?branch=master)](https://travis-ci.org/fortunalab/ontoavida)
[![DOI](https://zenodo.org/badge/13996/fortunalab/ontoavida.svg)](https://zenodo.org/badge/latestdoi/13996/fortunalab/ontoavida)

# OntoAvida: ontology for the Avida digital evolution platform.

The Ontology for Avida (OntoAvida) project aims to develop an integrated vocabulary for the description of [Avida](https://github.com/devosoft/avida), the most widely used computational approach for performing experimental evolution. The lack of a clearly defined vocabulary makes biologists feel reluctant to embrace the field of digital evolution. This unique ontology has the potential to change this picture overnight. In addition, OntoAvida will allow researchers to make inference (e.g., on phenotypic plasticity) based on certain rules and constraints, facilitate the reproducibility of the *in silico* evolution experiments reported in the scientific literature (see [references](#references) below), and trace the provenance of the data stored in AvidaDB&mdash;a RDF database on digital organisms and their genomes.

Digital evolution is a form of evolutionary computation in which self-replicating computer programs&mdash;digital organisms&mdash;evolve within a user-defined computational environment. Avida satisfies three essential requirements for evolution to occur: replication, heritable variation, and differential fitness. The latter arises through competition for the limited resources of memory space and central processing unit, CPU time. A digital organism in Avida consists of a sequence of instructions&mdash;its genome or genotype&mdash;and a virtual CPU, which executes these instructions. Some of these instructions are involved in copying an organism's genome, which is the only way the organism can pass its genetic material to future generations. To reproduce, a digital organism must copy this genome instruction by instruction into a new region of memory through a process that may lead to errors, i.e., mutations. A mutation occurs when an instruction is copied incorrectly, and is instead replaced in the offspring genome by an instruction chosen at random (with a uniform distribution) from a set of possible instructions. Some instructions are required for viability&mdash;replication&mdash;whereas others are required to complete computational operations (such as addition, multiplications, and bit-shifts), and are executed on binary numbers taken from the environment through input-output instructions harbored in the digital organism's genome. The output of processing these numbers might be the result of a specific Boolean logic operation (digital analogs of functional traits). The identity of the logic operations that can be computed by a digital organism defines it phenotype. An organism can be rewarded for computing Boolean logic operations with virtual CPU-cycles, which speeds up the execution of the instructions harbored in its genome. This creates an additional selective pressure (besides reducing the number of instructions required for replication) which favours those organisms in an evolving population where mutations have produced sequences of instructions in their genomes that encode phenotypes determined by the Boolean logic operations they compute. Organisms that are more successful&mdash;those that replicate faster&mdash;are more likely to spread through a population.

Many scientific publications have used Avida as digital evolution platform to shed light on fundamental questions in ecology and evolutionary biology, such as the evolution of genome architecture, gene regulatory networks, robustness, evolvability, complexity, phenotypic plasticity, adaptive radiations, ecological interactions, cooperation, and the evolution of sex. We provide an exhaustive [list of references](#references) at the end of this page.

OntoAvida was initially developed by the [computational biology lab](https://fortunalab.org) at the Doñana Biological Station (a research institute of the Spanish National Research Council based at Seville, Spain). Contributors to OntoAvida are expected to include members of the [Digital Evolution Laboratory](https://devolab.org/) at Michigan State University (USA).

We have uploaded a html version of OntoAvida at https://owl.fortunalab.org/ontoavida. More information can be found at http://obofoundry.org/ontology/ontoavida.html.

---

> #### **Subset of the ontology showing inference, reproducibility, and provenance:**
<div style="text-align:center"><img src="images/subset.png"></div>

---

> #### **Workflow for a new release (ROBOT commands):**
<div style="text-align:center"><img src="images/workflow.png"></div>

---
## Versions.

### Stable release versions.

The latest version of the ontology can always be found at:

http://purl.obolibrary.org/obo/ontoavida.owl.


### Editors' version.

Editors of this ontology should NOT use the edit version, [ontoavida-edit.owl](ontoavida-edit.owl). New terms (classes, object properties and data properties) should be added to the existing [template files](templates/), or [import files](imports/) if they come from external ontologies.

---

## Contact.

Please use this GitLab repository's [Issue tracker](https://gitlab.com/fortunalab/ontoavida/-/issues) to request new terms or report errors or specific concerns related to the ontology.

---

## Acknowledgements.

OntoAvida was created using [ROBOT](http://robot.obolibrary.org), a tool for working with [Open Biomedical Ontologies](http://obofoundry.org). Avida is currently developed by the [Digital Evolution Laboratory](https://devolab.org/) at Michigan State University (USA).

---

## References.
> **Scientific publications using Avida (sorted by date)**.
#### 2021
- Kumawat, B. and Bhat, R. (2021). An interplay of resource availability, population size and mutation rate potentiates the evolution of metabolic signaling. *BMC Ecol. Evol.*, 21:52. [doi](https://doi.org/10.1186/s12862-021-01782-0)

- Lalejini, A., Ferguson, A. J., Grant, N. A., and Ofria, C. (2021). Phenotypic plasticity stabilizes evolution in fluctuating environments. *Front. Ecol. Evol.*, 9:715381. [doi](https://doi.org/10.3389/fevo.2021.715381)

#### 2020
- Dolson, E., Lalejini, A., Jorgensen, S., and Ofria, C. (2020). Interpreting the tape of life: ancestry-based analyses provide insights and intuition about evolutionary dynamics. *Artif. Life*, 26:58-79. [doi](https://doi.org/10.1162/artl_a_00313)

#### 2019
- Dolson, E. L., Vostinar, A. E., Wiser, M. J., and Ofria, C. (2019). The MODES toolbox: measurements of open-ended dynamics in evolving systems. *Artif. Life*, 25:50-73. [doi](https://doi.org/10.1162/artl_a_00280)

- Franklin, J., LaBar, T., and Adami, C. (2019). Mapping the peaks: fitness landscapes of the fittest and the flattest. *Artif. Life*, 25:250-262. [doi](https://doi.org/10.1162/artl_a_00296)

#### 2017
- Connelly, B. D., Bruger, E. L., McKinley, P. K., and Waters, C. M. (2017). Resource abundance and the critical transition to cooperation. *J. Evol. Biol.*, 30:750-761. [doi](https://doi.org/10.1111/jeb.13039)

- Fortuna, M. A., Zaman, L., Ofria, C., and Wagner, A. (2017). The genotype-phenotype map of an evolving organism. *PLoS Comput. Biol.*, 13:e1005414. [doi](https://doi.org/10.1371/journal.pcbi.1005414)

- Fortuna, M. A., Zaman, L., Wagner, A., and Bascompte, J. (2017). Innovations increase network complexity in interacting digital organisms. *Phil. Trans. R. Soc. B.*, 372:20160431. [doi](https://doi.org/10.1098/rstb.2016.0431)

- Nitash, C. G., LaBar, T., Hintze, A., and Adami, C. (2017). Origin of life in a digital microcosm. *Philos. Trans. A. Math. Phys. Eng. Sci.*, 375:20160350. [doi](https://doi.org/10.1098/rsta.2016.0350)

#### 2016
- Gupta, A., LaBar, T., Miyagi, M., and Adami, C. (2016). Evolution of genome size in asexual digital organisms. *Sci. Rep.*, 6:25786. [doi](https://doi.org/10.1038/srep25786)

- LaBar, T., Hintze, A., and Adami, C. (2016). Evolvability tradeoffs in emergent digital replicators. *Artif. Life*, 22:483-498. [doi](https://doi.org/10.1162/artl_a_00214)

- Strona, G., and Lafferty, K. D. (2016). Environmental change makes robust ecological networks fragile. *Nat. Commun.*, 7:12462. [doi](http://dx.doi.org/10.1038/ncomms12462)

- Taylor, T., Auerbach, J. E., Bongard, J., Clune, J., Hickinbothan, S., Ofria, C., Oka, M., et al. (2016). WebAL comes of age: a review of the first 21 years of artificial life on the web. *Artif. Life*, 22:364–407. [doi](https://doi.org/10.1162/ARTL_a_00211)

- Taylor, T., Bedau, M., Channon, A., Ackley, D., Banzhaf, W., Beslon, G., Dolson, E., et al. (2016). Open-ended evolution: perspectives from the OEE Workshop in York. *Artif. Life*, 22:408–423. [doi](https://doi.org/10.1162/ARTL_a_00210)

#### 2015
- Ostrowski, E. A., Ofria, C., and Lenski, R. E. (2015). Genetically integrated traits and rugged adaptive landscapes in digital organisms. *BMC Evol. Biol.*, 15:83. [doi](https://doi.org/10.1186/s12862-015-0361-x)

#### 2014
- Goldsby, H. J., Knoester, D. B., Kerr, B., and Ofria, C. (2014). The effect of conflicting pressures on the evolution of division of labor. *PLoS One*, 9:e102713. [doi](https://doi.org/10.1371/journal.pone.0102713)

- Goldsby, H. J., Knoester, D. B., Ofria, C., and Kerr, B. (2014). The evolutionary origin of somatic cells under the dirty work hypothesis. *PLoS Biol.*, 12:e1001858. [doi](https://doi.org/10.1371/journal.pbio.1001858)

- Johnson, T. J. , Goldsby, H. J., Goings, S., and Ofria, C. (2014). The evolution of kin inclusivity levels. In *Proceedings of the Annual Conference on Genetic and Evolutionary Computation*, pp:177–184. [doi](https://doi.org/10.1145/2576768.2598283)

- Zaman, L., Meyer, J. R., Devangam, S., Bryson, D. M., Lenski, R. E., and Ofria, C. (2014). Coevolution drives the emergence of complex traits and promotes evolvability. *PLoS Biol.*, 12:e1002023. [doi](https://doi.org/10.1371/journal.pbio.1002023)

#### 2013
- Bryson, D. M. and Ofria, C. (2013). Understanding evolutionary potential in virtual CPU instruction set architectures. *PLoS One*, 8:83242. [doi](https://doi.org/10.1371/journal.pone.0083242)

- Covert, A. W., Lenski, R. E., Wilke, C. O., and Ofria, C. (2013). Experiments on the role of deleterious mutations as stepping stones in adaptive evolution. *PNAS*, 110:E3171–E3178. [doi](https://doi.org/10.1073/pnas.1313424110)

- Fortuna, M. A., Zaman, L., Wagner, A., and Ofria, C. (2013). Evolving digital ecological networks. *PLoS Comput. Biol.*, 9:e1002928. [doi](https://doi.org/10.1371/journal.pcbi.1002928)

- Grabowski, L. M., Bryson, D. M., Dyer, F. C., Pennock, R. T., and Ofria, C. (2013). A case study of the de novo evolution of a complex odometric behavior in digital organisms. *PLoS One*, 8:e60466. [doi](https://doi.org/10.1371/journal.pone.0060466)

#### 2012
- Bryson, D. M. and Ofria, C. (2012). Digital evolution exhibits surprising robustness to poor design decisions. *Artif. Life*, 13:19–26. [doi](https://doi.org/10.1162/978-0-262-31050-5-ch003)

- Clune, J., Pennock, R. T., Ofria, C., and Lenski, R. E. (2012). Ontogeny tends to recapitulate phylogeny in digital organisms. *Am. Nat.*, 180:E54–E63. [doi](https://doi.org/10.1086/666984)

- Chandler, C. H., Ofria, C., and Dworkin, I. (2012). Runaway sexual selection leads to good genes. *Evolution*, 67:110–119. [doi](https://doi.org/10.1111/j.1558-5646.2012.01750.x)

- Goings, S., Goldsby, H. J., Cheng, B. H. C., and Ofria, C. (2012). An ecology-based evolutionary algorithm to evolve solutions to complex problems. *Artif. Life*, 13:171–177. [doi](https://doi.org/10.1162/978-0-262-31050-5-ch024)

- Goldsby, H. J., Dornhaus, A., Kerr, B., and Ofria, C. (2012). Task-switching costs promote the evolution of division of labor and shifts in individuality. *PNAS*, 109:13686–13691. [doi](https://doi.org/10.1073/pnas.1202233109)

- Walker, B. L. and Ofria, C. (2012). Evolutionary potential is maximized at intermediate diversity levels. *Artif. Life*, 13:116–120. [doi](http://dx.doi.org/10.7551/978-0-262-31050-5-ch017)

- Yedid, G. and Heier, L. (2012). Effects of random and selective mass extinction on community composition in communities of digital organisms. In *Evolutionary biology: mechanisms and trends*, pp:43–64. Springer-Verlag. [doi](https://doi.org/10.1007/978-3-642-30425-5_3)

- Yedid, G., Stredwick, J., Ofria, C., and Agapow, P. M. (2012). A comparison of the effects of random and selective mass extinctions on erosion of evolutionary history in communities of digital organisms. *PLoS One*, 7:e37233. [doi](https://doi.org/10.1371/journal.pone.0037233)

- Zaman, L., Ofria, C., and Lenski, R. E. (2012). Finger-painting fitness landscapes: an interactive tool for exploring complex evolutionary dynamics. *Artif. Life*, 13:449–505. [doi](https://doi.org/10.1162/978-0-262-31050-5-ch065)

#### 2011
- Clune, J., Goldsby, H. J., Ofria, C., and Pennock, R. T. (2011). Selective pressures for accurate altruism targeting: evidence from digital evolution for difficult-to-test aspects of inclusive fitness theory. *Proc. R. Soc. B.*, 278:666–674. [doi](https://doi.org/10.1098/rspb.2010.1557)

- Connelly, B. D., Zaman, L., McKinley, P. K., Ofria, C. (2011). Modeling the evolutionary dynamics of plasmids in spatial populations. In *Proceedings of the Annual Conference on Genetic and Evolutionary Computation*, pp:227–233. [doi](https://doi.org/10.1145/2001576.2001608)

- Goldsby, H. J., Knoester, D. B., Clune, J., McKinley, P. K., and Ofria, C. (2011). The evolution of division of labor. In *Proceedings of the European Conference on Artificial Life*, pp:10–18. [doi](https://doi.org/10.1016/j.jtbi.2014.01.027)

- Zaman, L., Devangam, S., and Ofria, C. (2011). Rapid host-parasite coevolution drives the production and maintenance of diversity in digital organisms. In *Proceedings of the Annual Conference on Genetic and Evolutionary Computation*, pp:219–226. [doi](https://doi.org/10.1145/2001576.2001607)

#### 2010
- Connelly, B. D., Zaman, L., Ofria, C., and McKinley, P.K. (2010). Social structure and the maintenance of biodiversity. In *Proceedings of the International Conference on Artificial Life*, pp:461–468. [doi](http://www.cse.msu.edu/~mckinley/Pubs/files/Connelly.ALIFE.2010.pdf)

- Goldsby, H. J., Knoester, D. B., and Ofria, C. (2010). Evolution of division of labor in genetically homogeneous groups. In *Proceedings of the Annual Conference on Genetic and Evolutionary Computation*, pp:135–142. [doi](https://doi.org/10.1145/1830483.1830507)

- Grabowski, L. M., Bryson, D., Dyer, F. C., Ofria, C., and Pennock, R. T. (2010). Early evolution of memory usage in digital organisms. In *Proceedings of the International Conference on Artificial Life*, pp:224–231. [doi](http://citeseerx.ist.psu.edu/viewdoc/download;jsessionid=6CA5043F80C0E5C76A88581F17E6A8B1?doi=10.1.1.186.1564&rep=rep1&type=pdf)

- Misevic, D., Ofria, C., and Lenski, R. E. (2010). Experiments with digital organisms on the origin and maintenance of sex in changing environments. *J. Hered.*, 101:S46–S54. [doi](https://doi.org/10.1093/jhered/esq017)

- Strelioff, C. C., Lenski, R.E., and Ofria, C. (2010). Evolutionary dynamics, epistatic interactions, and biological information. *J. Theor. Biol.*, 266:584–594. [doi](https://doi.org/10.1016/j.jtbi.2010.07.025)

#### 2009
- Beckmann, B. E., Grabowski, L.M., McKinley, P. K., and Ofria, C. (2009). Applying digital evolution to the design of self-adaptive software. In *Proceedings of the EEE Symposium on Artificial Life*, pp:100–107. [doi](http://www.cse.msu.edu/~mckinley/Pubs/files/alife-2009-beckmann-robots.pdf)

- Elsberry, W. R., Grabowski, L. M., Ofria, C., and Pennock, R. T. (2009). Cockroaches, drunkards, and climbers: modeling the evolution of simple movement strategies using digital organisms. In *Proceedings of the IEEE Symposium on Artificial Life*, pp:92–99. [doi](http://www.cse.msu.edu/~ofria/pubs/2009ElsberryEtAl.pdf)

- Goings, S. and Ofria, C. (2009). Ecological approaches to diversity maintenance in evolutionary algorithms. In *Proceedings of the IEEE Symposium on Artificial Life*, pp:124–130. [doi](http://dx.doi.org/10.1109/ALIFE.2009.4937703)

- Swan, L. S. (2009). Synthesizing insight: artificial life as thought experimentation in biology. *Biol. Philos.*, 24:687–701. [doi](https://doi.org/10.1007/s10539-009-9156-z)

- Yedid, G., Ofria, C., and Lenski, R. E. (2009). Selective press extinctions, but not random pulse extinctions, cause delayed ecological recovery in communities of digital organisms. *Am. Nat.*, 173:E139–E154. [doi](https://doi.org/10.1086/597228)

#### 2008
- Beckmann, B. E, McKinley, P,K, and Ofria, C. (2008). Selection for group-level efficiency leads to self-regulation of population size. In *Proceedings of the Annual Conference on Genetic and Evolutionary Computation*, pp:185–192. [doi](https://doi.org/10.1145/1389095.1389126)

- Clune, J., Misevic, D., Ofria, C., Lenski, R. E., Elena, S. F., and Sanjuán, R. (2008). Natural selection fails to optimize mutation rates for long-term adaptation on rugged fitness landscapes. *PLoS Comput. Biol.*, 4:e1000187. [doi](https://doi.org/10.1371/journal.pcbi.1000187)

- Clune, J., Ofria, C., and Pennock, R. T. (2008). How a generative encoding fares as problem-regularity decreases. In *Proceedings of the International Conference on Parallel Problem Solving from Nature*, pp:358–367. [doi](https://doi.org/10.1007/978-3-540-87700-4_36)

- Elena, S. F. and Sanjuán, R. (2008). The effect of genetic robustness on evolvability in digital organisms. *BMC Evol. Biol.*, 8:284. [doi](https://doi.org/10.1186/1471-2148-8-284)

- Gerlee, P. and Lundh, T. (2008). The emergence of overlapping scale-free genetic architecture in digital organisms. *Artif. Life*, 14:265–275. [doi](https://doi.org/10.1162/artl.2008.14.3.14303)

- Knoester, D. B., McKinley, P. K., and Ofria, C. (2008). Cooperative network construction using digital germlines. In *Proceedings of the Annual Conference on Genetic and Evolutionary Computation*, pp:217–224. [doi](https://doi.org/10.1145/1389095.1389130)

- McKinley, P., Cheng, B. H. C., Ofria, C., Knoester, D., Beckmann, B., and Goldsby, H. J. (2008). Harnessing digital evolution. *Computer*:54–63. [doi](https://www.cse.msu.edu/~mckinley/digital-evolution.pdf)

- Ofria, C., Huang, W., and Torng, E. (2008). On the gradual evolution of complexity and the sudden emergence of complex features. *Artif. Life*, 14:255–263. [doi](https://doi.org/10.1162/artl.2008.14.3.14302)

- Yedid, G., Ofria, C., and Lenski, R. E. (2008). Historical and contingent factors affect re-evolution of a complex feature lost during mass extinction in communities of digital organisms. *J. Evol. Biol.*, 21:1335–1357. [doi](https://doi.org/10.1111/j.1420-9101.2008.01564.x)

#### 2007
- Beckmann, B. E., McKinley, P.K., Knoester, D.B., and Ofria, C. (2007). Evolution of cooperative information gathering in self-replicating digital organisms. In *Proceedings of the International Conference on Self-Adaptive and Self-Organizing Systems*, pp:65–76. [doi](https://doi.org/10.1109/SASO.2007.24)

- Clune, J., Ofria, C., and Pennock, R. T. (2007). Investigating the emergence of phenotypic plasticity in evolving digital organisms. In *Proceedings of the European Conference on Artificial Life*, pp:74–83. [doi](https://doi.org/10.1007/978-3-540-74913-4_8)

- Elena, S. F., Wilke, C. O., Ofria, C., and Lenski, R. E. (2007). Effects of population size and mutation rate on the evolution of mutational robustness. *Evolution*, 61:666–674. [doi](https://doi.org/10.1111/j.1558-5646.2007.00064.x)

- Hang, D. H., Torng, E., Ofria, C., Schmidt, T. M. (2007). The effect of natural selection on the performance of maximum parsimony. *BMC Evol. Biol.*, 7:94. [doi](https://doi.org/10.1186/1471-2148-7-94)

- Knoester, D. B., McKinley, P. K., Beckmann, B. E., and Ofria, C. (2007). Directed evolution of communication and cooperation in digital organisms. In *Proceedings of the European Conference on Artificial Life*, pp:384–394. [doi](https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.409.1339&rep=rep1&type=pdf)

- Knoester, D. B., McKinley, P. K., and Ofria, C. (2007). Using group selection to evolve leadership in populations of self-replicating digital organisms. In *Proceedings of the Annual Conference on Genetic and Evolutionary Computation*, pp:293–300. [doi](https://doi.org/10.1145/1276958.1277016)

- Ostrowski, E. A., Ofria, C., and Lenski, R. E. (2007). Ecological specialization and adaptive decay in digital organisms. *Am. Nat.*, 169:E1–E20. [doi](https://doi.org/10.1086/510211)

- Pennock, R. T. (2007). Models, simulations, instantiations, and evidence: the case of digital evolution. *J. Exp. Theor. Artif. Intell.*, 19:29–42. [doi](https://doi.org/10.1080/09528130601116113)

- Pennock, R. T. (2007). Learning evolution and the nature of science using evolutionary computing and artificial life. *McGill J. Edu.*, 42:211–224. [doi](https://mje.mcgill.ca/article/view/2220/1690)

#### 2006
- Adami, C. (2006). Digital genetics: unravelling the genetic basis of evolution. *Nat. Rev. Genet.*, 7:109–118. [doi](https://doi.org/10.1038/nrg1771)

- Lenski, R. E., Barrick, J. E., and Ofria, C. (2006). Balancing robustness and evolvability. *PLoS Biol.*, 12:E428. [doi](https://doi.org/10.1371/journal.pbio.0040428)

- Misevic, D., Ofria, C., and Lenski, R. (2006). Sexual reproduction reshapes the genetic architecture of digital organisms. *Proc. R. Soc. B.*, 273:457–464. [doi](https://dx.doi.org/10.1098%2Frspb.2005.3338)

#### 2005
- Gerlee, P. and Lundh, T. (2005). The genetic coding style of digital organisms. In *Proceedings of the European Conference on Artificial Life*, pp:854–863. [doi](https://doi.org/10.1007/11553090_86)

#### 2004
- Adami, C. and Wilke, C. O. (2004). Experiments in digital evolution. *Artif. Life*, 10:117–122. [doi](https://authors.library.caltech.edu/12381/1/ADAal04.pdf)

- Chow, S. S., Wilke, C. O., Ofria, C., and Lenski, R. E., and Adami, C. (2004). Adaptive radiation from resource competition in digital organisms. *Science*, 305:84–86. [doi](https://doi.org/10.1126/science.1096307)

- Edlund, J. A. and Adami, C. (2004). Evolution of robustness in digital organisms. *Artif. Life*, 10:167–179. [doi](https://doi.org/10.1162/106454604773563595)

- Goings, S., Clune, J., Ofria, C., and Pennock, R. T. (2004). Kin-selection: the rise and fall of kin-cheaters. In *Proceedings of the International Conference on Artificial Life*, pp:303–308. [doi](https://doi.org/10.7551/mitpress/1429.001.0001)

- Hagstrom, G. I., Hang, D. H., Ofria, C., and Torng, E. (2004). Using Avida to test the effects of natural selection on phylogenetic reconstruction methods. *Artif. Life*, 10:157–166. [doi](https://doi.org/10.1162/106454604773563586)

- Johnson, T. J. and Wilke, C. O. (2004). Evolution of resource competition between mutually dependent digital organisms. *Artif. Life*, 10:145–156. [doi](https://doi.org/10.1162/106454604773563577)

- Lenski, R. E. (2004). The future of evolutionary biology. *Ludus Vitalis*, 21:67–89. [doi](http://lenski.mmg.msu.edu/lenski/pdf/2004,%20LudusVitalis,%20Lenski.pdf)

- Li, Y. and Wilke, C. O. (2004). Digital evolution in time-dependent fitness landscapes. *Artif. Life*, 10:123–134. [doi](https://doi.org/10.1162/106454604773563559)

- Ofria, C. and Wilke, C. O. (2004). Avida: a software platform for research in computational evolutionary biology. *Artif. Life*, 10:191–229. [doi](https://doi.org/10.1162/106454604773563612)

- Wagenaar, D. A. and Adami, C. (2004). Influence of change, history, and adaptation on digital evolution. *Artif. Life*, 10:181–190. [doi](https://doi.org/10.1162/106454604773563603)

- White, J. S. and Adami, C. (2004). Bifurcation into functional niches in adaptation. *Artif. Life*, 10:135–144. [doi](https://doi.org/10.1162/106454604773563568)

#### 2003
- Cooper, T. and Ofria, C. (2003). Evolution of stable ecosystems in populations of digital organisms. In *Proceedings of the International Conference on Artificial Life*, pp:227–232. [doi](http://www.cse.msu.edu/~ofria/pubs/2002CooperOfria.pdf)

- Hang, D. H., Ofria, C., Schmidt, T. M., and Torng, E. (2003). The effect of natural selection on phylogeny reconstruction algorithms. In *Proceedings of the Annual Conference on Genetic and Evolutionary Computation*, pp:13–24. [doi](http://www.cse.msu.edu/~torng/Research/Pubs/DehuaNaturalSelection.pdf)

- Lenski, R. E., Ofria, C., Pennock, R. T., and Adami, C. (2003). The evolutionary origin of complex features. *Nature*, 423:139–144. [doi](https://doi.org/10.1038/nature01568)

- Ofria, C., Adami, C., and Collier, T. C. (2003). Selective pressures on genomes in molecular evolution. *J. Theor. Biol.*, 222:477–483. [doi](https://doi.org/10.1016/s0022-5193(03)00062-6)

- Wilke, C. O. (2003). Does the Red Queen reign in the kingdom of digital organisms? In *Proceedings of the European Conference on Artificial Life*, pp:405–414. [doi](https://arxiv.org/pdf/physics/0302046.pdf)

#### 2002
- Adami, C. (2002). Ab initio modeling of ecosystems with artificial life. *Nat. Resour. Model*, 15:133–145. [doi](https://arxiv.org/pdf/physics/0209081.pdf)

- Ofria, C, Adami, C., and Collier, T. C. (2002). Design of evolvable computer languages. *IEEE T. Evolut. Comput.*, 6:420–424. [doi](https://doi.org/10.1109/TEVC.2002.802442)

- Wilke, C. O. and Adami, C. (2002). The biology of digital organisms. *Trends Ecol. Evol.*, 17:528–532. [doi](https://doi.org/10.1016/S0169-5347(02)02612-5)

#### 2001
- Wilke, C. O., Wang, J. L., Ofria, C., Lenski, R. E., and Adami, C. (2001). Evolution of digital organisms at high mutation rates leads to survival of the flattest. *Nature*, 412:331–333. [doi](https://doi.org/10.1038/35085569)

#### 2000
- Adami, C., Ofria, C., and Collier, T. C. (2000). Evolution of biological complexity. *PNAS*, 97:4463–4468. [doi](https://doi.org/10.1073/pnas.97.9.4463)

#### 1999
- Lenski, R. E., Ofria, C., Collier, T. C., and Adami, C. (1999). Genome complexity, robustness and genetic interactions in digital organisms. *Nature*, 400:661–664. [doi](https://doi.org/10.1038/23245)

---
