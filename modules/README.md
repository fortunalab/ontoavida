## Create ontology modules by adding the new terms specified in the template files.

---

### convert template files into owl modules using ROBOT:
```
robot template \
    --template class_template.tsv \
    --prefix "FBcv: http://purl.obolibrary.org/obo/FBcv_" \
    --prefix "ONTOAVIDA: http://purl.obolibrary.org/obo/ONTOAVIDA_" \
    --output class_module.owl
```

```
robot template \
    --template object_property_template.tsv \
    --prefix "FBcv: http://purl.obolibrary.org/obo/FBcv_" \
    --prefix "GSSO: http://purl.obolibrary.org/obo/GSSO_" \
    --prefix "NCIT: http://purl.obolibrary.org/obo/NCIT_" \
    --prefix "ONTOAVIDA: http://purl.obolibrary.org/obo/ONTOAVIDA_" \
    --prefix "RO: http://purl.obolibrary.org/obo/RO_" \
    --prefix "STATO: http://purl.obolibrary.org/obo/STATO_" \
    --output object_property_module.owl
```

```
robot template \
    --template datatype_property_template.tsv \
    --prefix "FBcv: http://purl.obolibrary.org/obo/FBcv_" \
    --prefix "ONTOAVIDA: http://purl.obolibrary.org/obo/ONTOAVIDA_" \
    --prefix "GSSO: http://purl.obolibrary.org/obo/GSSO_" \
    --output datatype_property_module.owl
```

### annotate the template modules using ROBOT:
```
robot annotate \
    --input class_module.owl \
    --annotation http://purl.org/dc/elements/1.1/title "Ontology module from the class template" \
    --annotation http://purl.org/dc/elements/1.1/description "Ontology module to be added to the ontoavida-edit ontology" \
    --annotation http://purl.org/dc/terms/license "OntoAvida by https://fortunalab.org is licensed under CC BY 4.0." \
    --ontology-iri "https://gitlab.com/fortunalab/ontoavida/modules/class_module.owl-annotated.owl" \
    --output class_module-annotated.owl
```

```
robot annotate \
    --input object_property_module.owl \
    --annotation http://purl.org/dc/elements/1.1/title "Ontology module from the object_property template" \
    --annotation http://purl.org/dc/elements/1.1/description "Ontology module to be added to the ontoavida-edit ontology" \
    --annotation http://purl.org/dc/terms/license "OntoAvida by https://fortunalab.org is licensed under CC BY 4.0." \
    --ontology-iri "https://gitlab.com/fortunalab/ontoavida/modules/object_property_module.owl-annotated.owl" \
    --output object_property_module-annotated.owl
```

```
robot annotate \
    --input datatype_property_module.owl \
    --annotation http://purl.org/dc/elements/1.1/title "Ontology module from the datatype_property template" \
    --annotation http://purl.org/dc/elements/1.1/description "Ontology module to be added to the ontoavida-edit ontology" \
    --annotation http://purl.org/dc/terms/license "OntoAvida by https://fortunalab.org is licensed under CC BY 4.0." \
    --ontology-iri "https://gitlab.com/fortunalab/ontoavida/modules/datatype_property_module.owl-annotated.owl" \
    --output datatype_property_module-annotated.owl
```

---