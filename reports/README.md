## Perform quality control checks using ROBOT.

---

### on the imported modules:
```
robot report \
    --input ../imports/FBcv_imports.owl \
    --output quality_control_check_imports_FBcv.tsv
```

```
robot report \
    --input ../imports/gsso_imports.owl \
    --output quality_control_check_imports_gsso.tsv
```

```
robot report \
    --input ../imports/ncit_imports.owl \
    --output quality_control_check_imports_ncit.tsv
```

```
robot report \
    --input ../imports/ro_imports.owl \
    --output quality_control_check_imports_ro.tsv
```

```
robot report \
    --input ../imports/stato_imports.owl \
    --output quality_control_check_imports_stato.tsv
```    
 
### on the imported unannotated modules:
```
robot report \
    --input ../imports/FBcv_imports-annotated.owl \
    --output quality_control_check_imports_FBcv-annotated.tsv
```

```
robot report \
    --input ../imports/gsso_imports-unannotated.owl \
    --output quality_control_check_imports_gsso-unannotated.tsv
```

```
robot report \
    --input ../imports/ncit_imports-unannotated.owl \
    --output quality_control_check_imports_ncit-unannotated.tsv
```

```
robot report \
    --input ../imports/ro_imports-annotated.owl \
    --output quality_control_check_imports_ro-annotated.tsv
```

```
robot report \
    --input ../imports/ro_imports-annotated.owl \
    --output quality_control_check_imports_ro-unannotated.tsv
```

```
robot report \
    --input ../imports/stato_imports-unannotated.owl \
    --output quality_control_check_imports_stato-unannotated.tsv
```

### on the annotated template modules:
```
robot report \
    --input ../modules/class_module-annotated.owl \
    --output quality_control_check_class_module-annotated.tsv
```

```
robot report \
    --input ../modules/object_property_module-annotated.owl \
    --output quality_control_check_object_property_module-annotated.tsv
```

```
robot report \
    --input ../modules/datatype_property_module-annotated.owl \
    --output quality_control_check_datatype_property_module-annotated.tsv
```

### on the core ontology (file edited with Protege):
```
robot report \
    --input ../ontoavida-edit.owl \
    --output quality_control_check_ontoavida-edit.tsv
```

### on the merged ontology:
```
robot report \
    --input ../results/ontoavida-merged.owl \
    --output quality_control_check_ontoavida-merged.tsv
```    

#### on the latest version of the ontology:
```
robot report \
    --input ../results/ontoavida-annotated.owl \
    --output ontoavida-annotated_report.tsv
```

---
