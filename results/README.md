### Ontologies as output.

---

### merge modules with the core ontology using ROBOT:
```
robot merge \
    --input ../ontoavida-edit.owl \
    --input ../imports/FBcv_imports-annotated.owl \
    --input ../imports/gsso_imports-unannotated.owl \
    --input ../imports/ncit_imports-unannotated.owl \
    --input ../imports/ro_imports-annotated.owl \
    --input ../imports/stato_imports-unannotated.owl \
    --input ../modules/class_module-annotated.owl \
    --input ../modules/object_property_module-annotated.owl \
    --input ../modules/datatype_property_module-annotated.owl \
    --output ontoavida-merged.owl
```

### check the logical consistency of the ontology and perform automated classification of terms using ROBOT:

#### ontology reasoning:
```
robot reason --reasoner ELK \
    --input ontoavida-merged.owl \
    --output ontoavida-reasoned.owl
```

#### relax equivalent axioms:
```
robot relax \
    --input ontoavida-reasoned.owl \
    --output ontoavida-relaxed.owl
``` 

#### remove redundant axioms:
```
robot reduce \
    --reasoner ELK \
    --input ontoavida-relaxed.owl \
    --output ontoavida-reduced.owl
```

#### update annotations before releasing (e.g., dated version IRI):
```
robot annotate \
    --input ontoavida-reduced.owl \
    --version-iri "http://purl.obolibrary.org/obo/ontoavida-v2012-08-23.owl" \
    --output ontoavida-annotated.owl
```

---
