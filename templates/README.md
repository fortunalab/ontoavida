# Contributors can propose new terms by adding them in the following template files.

---

## Classes.

> *templates/class_template.tsv*

both, classes from internal and external ontologies proposed here will be added to the *ontoavida-edit.owl* file (i.e., the file will be empty after every new release).

## Object properties.

> *templates/object_property_template.tsv*

internal object properties proposed here will be added to the *ontoavida-edit.owl* file (i.e., the file will contain, after every new release, only object properties defined in external ontologies).

## Datatype properties.

> *templates/datatype_property_template.tsv*

datatype properties of internal classes proposed here will be added to the *ontoavida-edit.owl* file (i.e., the file will contain, after every new release, only the datatype properties of classes defined in external ontologies).

---